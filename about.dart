// ignore_for_file: deprecated_member_use, library_private_types_in_public_api

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unit Converter',
      home: const About(),
    );
  }
}

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('About'),
          ),
        body:
          Container(
            padding: const EdgeInsets.all(0.0),
            alignment: Alignment.center,
            child:
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const <Widget>[
                  Text(
                  "Unit converter",
                    style: TextStyle(fontSize:40.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
                  ),
                  SizedBox(
                    width: 200,
                    height: 20,
                  ),
                  Text(
                  "Version:",
                    style: TextStyle(fontSize:25.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
    
                  Text(
                  "1.0",
                    style: TextStyle(fontSize:15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
                  SizedBox(
                    width: 200,
                    height: 20,
                  ),
                  Text(
                  "Sub sections:",
                    style: TextStyle(fontSize:25.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
    
                  Text(
                  "3",
                    style: TextStyle(fontSize:15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
                  SizedBox(
                    width: 200,
                    height: 20,
                  ),
                  Text(
                  "Sub section names:",
                    style: TextStyle(fontSize:25.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
    
                  Text(
                  "Currency",
                    style: TextStyle(fontSize:15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
    
                  Text(
                  "Distance",
                    style: TextStyle(fontSize:15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
    
                  Text(
                  "Speed",
                    style: TextStyle(fontSize:15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
                  SizedBox(
                    width: 200,
                    height: 20,
                  ),
                  Text(
                  "Author:",
                    style: TextStyle(fontSize:25.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  ),
    
                  Text(
                  "Sekwayi Mokoena",
                    style: TextStyle(fontSize:15.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w200,
                    fontFamily: "Roboto"),
                  )
                ]
    
              ),
          ),
    
      );
    }
}