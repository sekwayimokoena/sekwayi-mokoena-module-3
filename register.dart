import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController txtName= TextEditingController();
    TextEditingController txtSurname = TextEditingController();
    TextEditingController txtEmail = TextEditingController();
    TextEditingController txtPassword = TextEditingController();
    var name = '';
    var surname = '';
    var email = '';
    var password = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: const Text('Register'),
          ),
        body:
          Container(
            padding: const EdgeInsets.fromLTRB(50.0, 50.0, 50.0, 10.0),
            alignment: Alignment.center,
            child:
               Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const <Widget>[
                      Text(
                      "Details",
                        style: TextStyle(fontSize:50.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Roboto"),
                      )
                    ]
                  ),
                  const SizedBox(
                      width: 300,
                      height: 20,
                      ),
                  TextField(
                    autofocus: true,
                    controller: txtName,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.account_box,),
                      border: OutlineInputBorder(),
                      hintText: "Name",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtSurname,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.account_box),
                      border: OutlineInputBorder(),
                      hintText: "Surname",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      border: OutlineInputBorder(),
                      hintText: "Address",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                    TextField(
                    controller: txtPassword,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      border: OutlineInputBorder(),
                      hintText: "Cellphone",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 30,
                    ),
                  SizedBox(
                    width: 200,
                      height: 30,
                    child: RaisedButton(key:null, onPressed: _save,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Save",
                          style: TextStyle(fontSize:20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w100,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    width: 200,
                      height: 30,
                    child: RaisedButton(key:null, onPressed: _clear,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Clear",
                          style: TextStyle(fontSize:20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w100,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                ]),
          ),
    
      );
  }
  void _save(){
    name = txtName.text;
    surname = txtSurname.text;
    email = txtEmail.text;
    password = txtPassword.text;
  }
  void _clear(){
    txtName.text = '';
    txtSurname.text = '';
    txtEmail.text = '';
    txtPassword.text = '';
  }
}