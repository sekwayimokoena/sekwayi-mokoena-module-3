// ignore_for_file: deprecated_member_use, library_private_types_in_public_api, sort_child_properties_last

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unit Converter',
      home: const Currency(),
    );
  }
}

class Currency extends StatefulWidget {
  const Currency({Key? key}) : super(key: key);
  @override
  _CurrencyState createState() => _CurrencyState();
}

class _CurrencyState extends State<Currency> {

    TextEditingController txtCurrency = TextEditingController();
    TextEditingController txtAnswer = TextEditingController();
    String answer = '';
    static const rate = 15;
    double dblAnswer = 0;

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Currency'),
          ),
        body:
          Container(
            child:
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const Text(
                  "ZAR: ",
                    style: TextStyle(fontSize:30.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
                  ),
                  
                  TextField(
                    controller: txtCurrency,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.money),
                      border: OutlineInputBorder(),
                      hintText: "Enter South African Rand value",
                    ),
                  ),
    
                  const Text(
                  "USA: ",
                    style: TextStyle(fontSize:30.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
                  ),
                  //12346569887785
                  TextField(
                    controller: txtAnswer,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.money),
                      border: OutlineInputBorder(),
                    ),
                    enabled: false,
                  ),

                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  SizedBox(
                    width: 200,
                      height: 30,
                    child: RaisedButton(key:null, onPressed:buttonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Calculate",
                          style: TextStyle(fontSize:25.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                    const SizedBox(
                      width: 300,
                      height: 50,
                    ),
                    SizedBox(
                      width: 200,
                      height: 30,
                      child: RaisedButton(key:null, onPressed:popButtonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Home",
                          style: TextStyle(fontSize:25.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        ),
                      ),
                    ),
                      const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                ],
    
              ),
    
            padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
            alignment: Alignment.center,
          ),
      );
    }
    void buttonPressed(){
      double currency = double.parse(txtCurrency.text);
      dblAnswer = currency / rate;
      answer = dblAnswer.toString();
      txtAnswer.text = answer;
      }
    
    void popButtonPressed(){
      Navigator.pop(context);
    }
}