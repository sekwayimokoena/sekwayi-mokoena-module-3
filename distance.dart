// ignore_for_file: deprecated_member_use, library_private_types_in_public_api

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unit Converter',
      home: const Distance(),
    );
  }
}

class Distance extends StatefulWidget {
  const Distance({Key? key}) : super(key: key);
  @override
  _DistanceState createState() => _DistanceState();
}

class _DistanceState extends State<Distance> {
  
    TextEditingController txtDistance = TextEditingController();
    TextEditingController txtAnswer = TextEditingController();
    String answer = '';

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Distance'),
          ),
        body:
          Container(
            padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
            alignment: Alignment.center,
            child:
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const Text(
                  "Kilometers: ",
                    style: TextStyle(fontSize:30.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
                  ),
                  
                  TextField(
                    controller: txtDistance,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.drive_eta),
                      border: OutlineInputBorder(),
                      hintText: "Enter kilometer value",
                    ),
                  ),
    
                  const Text(
                  "Miles: ",
                    style: TextStyle(fontSize:30.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
                  ),
    
                  TextField(
                    controller: txtAnswer,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.drive_eta),
                      border: OutlineInputBorder(),
                    ),
                    enabled: false
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  SizedBox(
                    width: 200,
                      height: 30,
                    child: RaisedButton(key:null, onPressed:buttonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Calculate",
                          style: TextStyle(fontSize:25.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                    const SizedBox(
                      width: 300,
                      height: 50,
                    ),
                    SizedBox(
                      width: 200,
                      height: 30,
                      child: RaisedButton(key:null, onPressed:popButtonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Home",
                          style: TextStyle(fontSize:25.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        ),
                      ),
                    ),
                      const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                ],
    
              ),
          ),
      );
    }
    void buttonPressed(){
      double distance = double.parse(txtDistance.text);
      const rate = 1.7;
      double dblAnswer;
      
      dblAnswer = distance / rate;
      answer = dblAnswer.toString();
      txtAnswer.text = answer;
    }

    void popButtonPressed(){
      Navigator.pop(context);
    }
    
}