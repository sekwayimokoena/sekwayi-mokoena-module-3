// ignore_for_file: library_private_types_in_public_api, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:two_developer/main.dart';
import 'package:two_developer/register.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unit Converter',
      home: const LoginScreen(),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

    TextEditingController txtEmail = TextEditingController();
    TextEditingController txtPassword = TextEditingController();

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: const Text('Unit converter'),
          ),
        body:
          Container(
            padding: const EdgeInsets.fromLTRB(50.0, 150.0, 50.0, 100.0),
            alignment: Alignment.center,
            child:
               Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const <Widget>[
                      Text(
                      "Login",
                        style: TextStyle(fontSize:50.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Roboto"),
                      )
                    ]
                  ),
                  const SizedBox(
                      width: 300,
                      height: 20,
                      ),
                  TextField(
                    controller: txtEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.email,),
                      border: OutlineInputBorder(),
                      hintText: "Username",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtPassword,
                    obscureText: true,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      border: OutlineInputBorder(),
                      hintText: "Password",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 30,
                    ),
                  SizedBox(
                    width: 200,
                      height: 30,
                    child: RaisedButton(key:null, onPressed:buttonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Signin",
                          style: TextStyle(fontSize:20.0,
                          color: Color(0xFF000000),
                          fontWeight: FontWeight.w100,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  SizedBox(
                    width: 200,
                      height: 30,
                    child: RaisedButton(key:null, onPressed:_register,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Register",
                          style: TextStyle(fontSize:20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w100,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                ],
              ),
          ),
    
      );
    }
    void buttonPressed(){
      String email = txtEmail.text;
      String password = txtPassword.text;
      String correctEmail = 'sekwayi@gmail.com';
      String correctPassword = 'Emma140919';

      if (email == correctEmail && password == correctPassword){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
          return const MyHomePage();
        }),);
      }else{
        showDialog(
        context: context,
        builder: (_) => const AlertDialog(
        title: Text('Incorrect'),
        content: Text('You have entered an incorrect email and/or password'),
        ),
        barrierDismissible: true,
      );
      }
    }
  void _register(){
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
          return const Register();
        }),);
  }
    
}