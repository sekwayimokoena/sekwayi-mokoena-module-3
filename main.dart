// ignore_for_file: deprecated_member_use, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:two_developer/about.dart';
import 'package:two_developer/currency.dart';
import 'package:two_developer/distance.dart';
import 'package:two_developer/login.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unit Converter',
      home: const LoginScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  // ignore: library_private_types_in_public_api
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Unit converter'),
          ),
        body:
          Container(
            padding: const EdgeInsets.fromLTRB(20.0, 150.0, 20.0, 10.0),
            alignment: Alignment.center,
            child:
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const Text(
                  "Select what you want to convert",
                    style: TextStyle(fontSize:20.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
                  ),
                  Container(
                      height: 20,
                  ),
                  SizedBox(
                    width: 200,
                    height: 30,
                    child: RaisedButton(key:null, onPressed:currencyButtonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Currency",
                          style: TextStyle(fontSize:20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        ),
                      ),
                  ),
                  const SizedBox(
                    width: 200,
                      height: 10,
                  ),
                  SizedBox(
                    width: 200,
                    height: 30,
                    child: RaisedButton(key:null, onPressed:distanceButtonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "Distance",
                          style: TextStyle(fontSize:20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        )
                      ),
                  ),
                  const SizedBox(
                    width: 200,
                    height: 20,
                  ),
                  SizedBox(
                    width: 200,
                    height: 30,
                    child: RaisedButton(key:null, onPressed:aboutButtonPressed,
                      color: Colors.grey,
                      child:
                        const Text(
                        "About",
                          style: TextStyle(fontSize:20.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                        )
                      ),
                  ),
                ]
    
              ),
          ),
    
      );
    }
    void buttonPressed(){}

    void currencyButtonPressed(){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
          return const Currency();
        }),);
    }

    void distanceButtonPressed(){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
          return const Distance();
        }),);
    }

    void aboutButtonPressed(){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
          return const About();
        }),);
    }
    
}